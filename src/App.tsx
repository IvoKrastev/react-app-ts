import Table from "./components/Table/Table";
import { Wrapper } from './App.style'

function App() {
  return (
    <Wrapper>
      <Table />
    </Wrapper>
  );
}

export default App;
