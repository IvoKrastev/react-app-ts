import { gql } from "@apollo/client";

const CountryQuery = gql`
    query {
        countries {
            name
            capital
            emoji
            currency  
        }
    }
`;

export default CountryQuery;
