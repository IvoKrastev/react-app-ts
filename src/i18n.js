import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const resources = {
  en: {
    translation: {
      "Welcome to React": "Welcome to React and react-i18next",
      "language": "Language",
      "countries": "Countries:",
      "name": "Name",
      "label": "Label",
      "flag": "Flag",
      "currency": "Currency",
      "action": "Action",
      "next": "Next",
      "prev": "Prev"
    }
  },
  cz: {
    translation: {
      "Welcome to React": "Bienvenue à React et react-i18next",
      "language": "Jazyk",
      "countries": "Země:",
      "name": "Název",
      "label": "Označení",
      "flag": "Vlajka",
      "currency": "Měna",
      "action": "Akce",
      "next": "Další",
      "prev": "Předchozí"
    }
  }
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",
    interpolation: {
      escapeValue: false 
    }
  });

  export default i18n;