import styled from "styled-components";

export const OptionsWrap = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  align-items: center;
`;

export const OptionsTitle = styled.h1`
  color: #7dc0fc;
  text-shadow: 1px 1px 1px #ddd;
`;

export const LanguageLabel = styled.p`
  color: #7dc0fc;
`;

export const LanguageSelect = styled.select`
  color: #7dc0fc;
  width: 100%;
  margin: 5px 0px;
  border: 1px solid #7dc0fc;
  border-radius: 5px;
  padding: 2px;
`;