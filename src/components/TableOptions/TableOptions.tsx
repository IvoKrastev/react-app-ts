import { useTranslation } from "react-i18next";

import {
  OptionsWrap,
  OptionsTitle,
  LanguageLabel,
  LanguageSelect,
} from "./TableOptions.style";

import i18n from "../../i18n";

type Props = {
  countriesCount: number;
};

function TableOptions({ countriesCount }: Props) {
  const { t } = useTranslation();

  return (
    <OptionsWrap>
      <OptionsTitle>{`${t("countries")} ${countriesCount}`}</OptionsTitle>
      <div>
        <LanguageLabel>{t("language")}</LanguageLabel>
        <LanguageSelect onChange={(e) => i18n.changeLanguage(e.target.value)}>
          <option value="en">EN</option>
          <option value="cz">CZ</option>
        </LanguageSelect>
      </div>
    </OptionsWrap>
  );
}

export default TableOptions;
