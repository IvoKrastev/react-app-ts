import styled from "styled-components";

export const TableWrapper = styled.div`
  width: 800px;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const Thead = styled.thead`
  background-color: #7cc1fc;
  text-align: left;
`;

export const TheadRow = styled.tr`
  border: 1px solid #b6b6b6;
`;

export const TheadTitle = styled.th`
  padding: 10px;
  font-size: 20px;
  text-transform: capitalize;
  color: #fff;
`;

export const TableBody = styled.tbody``;

export const TBodyRow = styled.tr`
  border: 1px solid #b6b6b6;
  &:nth-child(even) {
    background-color: #e5e5e5;
  }
  &:hover {
    background: #b5b5b5;
  }
`;

export const TBodyItem = styled.td`
    padding: 8px;
    font-size: 18px;
    width: min-content;
`;

export const Message = styled.p`
  font-size: 24px;
`;
