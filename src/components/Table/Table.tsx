import { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";
import CountryQuery from "../../Queries/CountryQuery.api";

import Pagination from "../Pagination/Pagination";
import TableOptions from "../TableOptions/TableOptions";

import {
  TableWrapper,
  Message,
  Table,
  Thead,
  TheadRow,
  TheadTitle,
  TableBody,
  TBodyRow,
  TBodyItem,
} from "./Table.style";

interface Country {
  name: string;
  capital: string;
  emoji: any;
  currency: string;
}

interface Data {
  countries: Country[];
}

interface Page {
  start: number;
  end: number;
}

function CountryTable() {
  const { t } = useTranslation();

  const tableHeader = [
    t("name"),
    t("label"),
    t("flag"),
    t("currency"),
    t("action"),
  ];
  const countryPerPage = 10;

  const [countries, setCountries] = useState<Country[]>([]);
  const [slicedCountries, setSlicedCountries] = useState<Country[]>([]);
  const [page, setPage] = useState<Page>({ start: 0, end: countryPerPage });

  const { data, loading, error } = useQuery<Data>(CountryQuery);

  useEffect(() => {
    if (data) {
      setCountries(data.countries);
      setSlicedCountries(data.countries.slice(page.start, page.end));
    }
  // eslint-disable-next-line
  }, [data]);

  if (loading) return <Message>Loading...</Message>;
  if (error)
    return <Message>Something went wrong. :( Please try again later</Message>;

  const deleteHandler = (selectedName: string) => {
    const filteredSliced = slicedCountries.filter(
      ({ name }) => name !== selectedName
    );
    const filtered = countries.filter(({ name }) => name !== selectedName);
    setSlicedCountries(filteredSliced);
    setCountries(filtered);
  };

  return (
    <TableWrapper>
      <TableOptions countriesCount={countries.length} />
      <Table>
        <Thead>
          <TheadRow>
            {tableHeader.map((item) => (
              <TheadTitle key={item}>{item}</TheadTitle>
            ))}
          </TheadRow>
        </Thead>
        <TableBody>
          {slicedCountries?.map(
            ({ name, capital, emoji, currency }: Country) => (
              <TBodyRow key={name}>
                <TBodyItem>{name}</TBodyItem>
                <TBodyItem>{capital}</TBodyItem>
                <TBodyItem>{emoji}</TBodyItem>
                <TBodyItem>{currency}</TBodyItem>
                <TBodyItem
                  style={{ cursor: "pointer", textAlign: "center" }}
                  onClick={() => deleteHandler(name)}
                >
                  &#128465;
                </TBodyItem>
              </TBodyRow>
            )
          )}
        </TableBody>
      </Table>
      <Pagination
        countryPerPage={countryPerPage}
        page={page}
        setPage={setPage}
        countries={countries}
        setSlicedCountries={setSlicedCountries}
      />
    </TableWrapper>
  );
}

export default CountryTable;
