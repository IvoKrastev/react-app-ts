import React from "react";
import { useTranslation } from "react-i18next";

import { Button, BtnWrap } from "./Pagination.style";

enum PageOptions {
  plus = "PLUS",
  minus = "MINUS",
}

type Page = {
  start: number;
  end: number;
};

interface Country {
  name: string;
  capital: string;
  emoji: any;
  currency: string;
}

type Props = {
  setSlicedCountries: (slicedCountries: Country[]) => void;
  countries: Country[];
  page: Page;
  setPage: (page: Page) => void;
  countryPerPage: number;
};

function Pagination({
  setSlicedCountries,
  countries,
  page,
  setPage,
  countryPerPage,
}: Props) {
  const { t } = useTranslation();

  const pageHandler = (operator: PageOptions) => {
    // Simple paginating implementation
    // Would be better to use apollo fetchMore(), limit, ofset etc. if available in GraphQL API or UI library

    if (operator === PageOptions.plus) {
      setSlicedCountries(
        countries.slice(page.start + countryPerPage, page.end + countryPerPage)
      );
      setPage({
        start: page.start + countryPerPage,
        end: page.end + countryPerPage,
      });
    } else {
      setSlicedCountries(
        countries.slice(page.start - countryPerPage, page.end - countryPerPage)
      );
      setPage({
        start: page.start - countryPerPage,
        end: page.end - countryPerPage,
      });
    }
  };

  return (
    <BtnWrap>
      <Button
        isDisabled={page.start === 0}
        disabled={page.start === 0}
        onClick={() => pageHandler(PageOptions.minus)}
      >
        {t("prev")}
      </Button>
      <Button
        isDisabled={page.end >= countries.length}
        disabled={page.end >= countries.length}
        onClick={() => pageHandler(PageOptions.plus)}
      >
        {t("next")}
      </Button>
    </BtnWrap>
  );
}

export default Pagination;
