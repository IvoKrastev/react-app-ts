import styled from "styled-components";

export const BtnWrap = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-top: 10px;
`

export const Button = styled.button<{isDisabled: boolean}>`
  padding: 8px;
  width: 100px;
  border: none;
  border-radius: 5px;
  box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s;
  background-color: ${props => (props.isDisabled ? `#d5d5d5` : `#7cc1fc`)};;
  color: #fff;
  text-transform: uppercase;
  cursor: pointer;
  &:hover{
    transform: scale(1.2);
  }
`;